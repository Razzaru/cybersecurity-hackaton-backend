<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsAnsweredRightToUserTestQuestionAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_test_question_answer', function (Blueprint $table) {
            $table->boolean('is_answered_right')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_test_question_answer', function (Blueprint $table) {
            $table->dropColumn('is_answered_right');
        });
    }
}
