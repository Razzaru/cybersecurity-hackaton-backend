<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function get(Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $request->user();

        $categories = Category::get(['id', 'name'])->map(function (Category $category) use ($currentUser) {
            $category->list = $category->lessons()->get(['id', 'name']);
            $category->competedCount = $currentUser ? $currentUser->lessons()->where('category_id', $category->id)->count() : 0;

            return $category;
        });

        return [
            'count' => Lesson::count(),
            'competedCount' => $currentUser ? $currentUser->lessons()->count() : 0,
            'categories' => $categories
        ];
    }

    public function getOne($id)
    {
        /** @var Lesson $lesson */
        $lesson = Lesson::find($id);
        $lesson->lessonPages = $lesson->lessonPages()->orderBy('page_number')->get(['id', 'content', 'page_number']);

        return response()->json($lesson);
    }

    public function saveUserLesson(Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $request->user();

        $newUserLesson = $currentUser->lessons()->create([
            'lesson_id' => $request->id
        ]);

        if ($newUserLesson) {
            return [
                'achievements' => [],
                'user' => User::find($currentUser->id)
            ];
        }
    }
}
