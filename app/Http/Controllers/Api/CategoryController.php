<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function get(Request $request)
    {
        $categories = Category::all();

        return [
            'count' => $categories->count(),
            'categories' => $categories,
        ];
    }

    public function getOne($id)
    {
        /** @var Category $lesson */
        $category = Category::find($id);

        return response()->json($category);
    }

    public function create(Request $request)
    {
        $category = new Category($request->toArray());
        $category->saveOrFail();

        return response()->json([
            'message' => 'Категория успешно создана!'
        ]);
    }

    public function update(Request $request, $id) {
        $category = Category::findOrFail($id);
        $category->update($request->toArray());

        return response()->json([
            'message' => 'Категория успешно обновлена!'
        ]);
    }

    public function delete($id) {
        Category::findOrFail($id)->delete();

        return response()->json([
            'message' => 'Категория успешно удалена!'
        ]);
    }
}
