<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Category;
use App\Models\Test;
use App\Models\TestQuestion;
use App\Models\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function get(Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $request->user();

        $categories = Category::get(['id', 'name'])->map(function (Category $category) use ($currentUser) {
            $category->list = $category->tests()->get(['id', 'name'])->map(function ($test) use ($currentUser) {
                $test->rightAnswerCount = 10;

                return $test;
            });
            $category->competedCount = $currentUser ? $currentUser->tests()->where('category_id', $category->id)->count() : 0;

            return $category;
        });

        return [
            'count' => Test::count(),
            'competedCount' => $currentUser ? $currentUser->tests()->count() : 0,
            'categories' => $categories
        ];
    }

    public function getOne($id)
    {
        /** @var Test $test */
        $test = Test::find($id);
        $test->testQuestions = $test
            ->testQuestions()
            ->inRandomOrder()
            ->limit(15)
            ->get(['id', 'question'])
            ->map(function (TestQuestion $testQuestion) {
                $testQuestion->testQuestionAnswers = $testQuestion
                    ->testQuestionAnswers()
                    ->inRandomOrder()
                    ->get(['id', 'name', 'is_right']);

                return $testQuestion;
            });

        return response()->json($test);
    }

    public function saveUserTest(Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $request->user();

        $newUserLesson = $currentUser->tests()->create([
            'test_id' => $request->id
        ]);

        foreach ($request->answers as $answer) {
            $currentUser->testQuestionAnswers()->attach($answer->id, ['is_answered_right' => $answer->is_answered_right]);
        }

        if ($newUserLesson) {
            return [
                'achievements' => [],
                'user' => new UserResource(User::find($currentUser->id)),
            ];
        }

        return [];
    }
}
