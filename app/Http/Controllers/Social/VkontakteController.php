<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class VkontakteController extends Controller
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function callback(Request $request)
    {
        $code = $request->code();

        try {
            $this->client->get('GET', 'https://oauth.vk.com/access_token', [
//                'client_id' => ,
//                'client_secret' => ,
//                'redirect_uri' => ,
//                'code' => ,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Произошла ошибка!'
            ], 500);
        }
    }
}
