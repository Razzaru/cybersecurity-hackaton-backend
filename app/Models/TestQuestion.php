<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TestQuestion extends Model
{
    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }

    public function testQuestionAnswers(): HasMany
    {
        return $this->hasMany(TestQuestionAnswer::class);
    }
}
