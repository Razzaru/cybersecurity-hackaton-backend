<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Achievement extends Model
{
    public function achievementRank(): BelongsTo
    {
        return $this->belongsTo(AchievementRank::class);
    }
}
