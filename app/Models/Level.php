<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Level extends Model
{
    public function getLvl(): int
    {
        return self::where('end_exp', '<', $this->end_exp)->count() + 1;
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
