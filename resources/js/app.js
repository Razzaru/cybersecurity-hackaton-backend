import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Router} from "react-router-dom";
import App from "./components/App";
import {createBrowserHistory} from "history";

require('./bootstrap');

ReactDOM.render(
    <BrowserRouter>
        <Router history={createBrowserHistory()}>
            <App/>
        </Router>
    </BrowserRouter>,
    document.getElementById('root')
);
