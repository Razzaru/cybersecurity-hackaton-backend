import axios from "axios";

export const login = (data, setUser) => {
    axios.post(`/api/auth/login`, data)
        .then(response => {
            const token = response.data.access_token,
                user = response.data.user;

            localStorage.setItem('token', token);
            setUser(user);
            window.axios.defaults.headers.common['Authorization'] = `Bearer ` + token;
        })
        .catch(() => {

        });
};

export const user = (setUser) => {
    axios.get(`/api/auth/user`)
        .then(response => {
            const user = response.data.user;
            setUser(user);
        })
        .catch(() => {

        });
};
