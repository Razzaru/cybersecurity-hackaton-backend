import React from 'react';

export default class SessionLayout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
        };

        this.disableLoading = this.disableLoading.bind(this);
    }

    disableLoading() {
        this.setState(() => ({ loading: false }));
    }

    render() {
        const Component = this.props.component;
        const { route, setUser } = this.props;

        return (
            <div className="session-wrapper">
                <Component setUser={setUser} route={route}/>
            </div>
        );
    }
}
