import React from 'react';
import {Link} from 'react-router-dom';

export default class PublicLayout extends React.Component {
    render() {
        const Component = this.props.component;
        const route = this.props.route;

        return (
            <div>
                <Component route={route}/>
            </div>
        );
    }
}
