import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import privateRoutes from "../routes/privateRoutes";
import Forbidden from "../components/Forbidden";
import {Avatar, Card, Layout, Menu, PageHeader} from "antd";

const {Content, Sider, Header} = Layout;

export default class PrivateLayout extends React.Component {
    defaultSelectedLink() {
        const {path} = this.props.route.match;

        return Object.keys(privateRoutes).filter(route => {
            let active = false;

            if (privateRoutes[route].children) {
                active = privateRoutes[route].children.filter(d => d === path).length > 0;
            }

            return active || privateRoutes[route].path === path;
        });
    }

    render() {
        const { route, routeKey, user } = this.props;
        const Component = this.props.component;
        const ToolsName = (privateRoutes[routeKey] || {}).tools;
        const ToolsComponent = ToolsName ? (
            <ToolsName params={route.match.params}/>
        ) : (
            <PageHeader onBack={() => null} title={privateRoutes[routeKey].title}/>
        );
        let listOfRoutes = [];

        for (let name in privateRoutes) {
            if (!privateRoutes[name].disableInMenu && !privateRoutes[name].disableInMenu && (!privateRoutes[name].can || privateRoutes[name].can() !== false)) {
                listOfRoutes.push((
                    <Menu.Item key={name}>
                        <NavLink to={privateRoutes[name].path}>{privateRoutes[name].title}</NavLink>
                    </Menu.Item>
                ));
            }
        }

        return (
            <Layout>
                <Sider style={{
                    overflow: 'auto', height: '100vh', position: 'fixed', left: 0,
                }}>
                    <Menu defaultSelectedKeys={this.defaultSelectedLink()}
                          mode={'inline'}
                          theme={'dark'}
                          inlineCollapsed={false}
                          style={{
                              height: '100%',
                          }}
                    >
                        {listOfRoutes}
                    </Menu>
                </Sider>

                <Layout style={{marginLeft: 200, height: '100vh', flexDirection: 'column'}}>
                    <Header className={'header'} style={{background: '#fff'}}>
                        <div className="logo">
                            <h1>Cyberian Project</h1>
                        </div>

                        <div className={'panel'}>
                            <div className={'profile'}>
                                <Avatar>{user.name}</Avatar>
                            </div>
                        </div>
                    </Header>
                    <Content style={{height: '100vh', margin: '16px', overflow: 'initial'}}>
                        <Card
                            title={ToolsComponent}
                            style={{width: '100%', height: '100%', overflowY: 'auto'}}
                        >
                            {
                                <Component user={user} route={route}/>
                            }
                        </Card>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}
