import Login from '../components/session/Login';

export default {
    Login: {
        component: Login,
        path: '/login'
    },
};
