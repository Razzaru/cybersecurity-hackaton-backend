import Dashboard from "../components/private/Dashboard/Dashboard";
import CategoryView from "../components/private/Category/CategoryView";
import Category from "../components/private/Category/Category";


import Forbidden from "../components/Forbidden";
import NotFound from "../components/NotFound";

const privatePrefix = '/admin';

export default {
    Dashboard: {
        component: Dashboard,
        path: `${privatePrefix}/`,
        title: 'Доска',
        disableInMenu: false,
    },
    CategoryView: {
        component: CategoryView,
        path: `${privatePrefix}/category`,
        title: 'Категории',
        disableInMenu: false,
    },
    Category: {
        component: Category,
        path: `${privatePrefix}/category/:handle`,
        title: 'Категории',
        disableInMenu: true,
    },
    Forbidden: {
        component: Forbidden,
        path: `${privatePrefix}/forbidden`,
        title: 'Нет доступа',
        disableInMenu: true,
    },
    NotFound: {
        component: NotFound,
        path: `${privatePrefix}/404`,
        title: 'Страница не найдена',
        disableInMenu: true,
    },
};
