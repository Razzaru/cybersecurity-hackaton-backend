import Home from '../components/public/Home';
import PriceList from "../components/public/Pricelist";

export default {
    Home: {
        component: Home,
        path: '/'
    },
    PriceList: {
        component: PriceList,
        path: '/price-list'
    }
};
