import React from 'react';
import 'antd/lib/date-picker/style/css';
import { Icon } from 'antd';
import {login} from "../../actions/auth";

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    handleSubmit(e) {
        const { email, password } = this.state;
        const { setUser } = this.props;

        e.preventDefault();
        login({ email, password }, setUser);
    }

    onChange(field, value) {
        this.setState(() => ({
            [field]: value,
        }));
    }

    render() {
        const { email, password } = this.state;

        return (
            <div className={'login-wrapper'}>
                <div className="container-login100">
                    <div className="wrap-login100">
                        <form className="login100-form validate-form" onSubmit={this.handleSubmit}>
                                <span className="login100-form-logo">
                                    <img src="/vectors/main-logo.svg" alt="Logotype"/>
                                </span>

                            <div className="wrap-input100 validate-input" data-validate="Enter username">
                                <input className="input100" type="text" name="email" placeholder="E-mail" value={email}
                                       onChange={(e) => {
                                           this.onChange('email', e.target.value)
                                       }}/>
                                <span className="focus-input100" data-placeholder="">
                                    <Icon type="mail"/>
                                </span>
                            </div>

                            <div className="wrap-input100 validate-input" data-validate="Enter password">
                                <input className="input100" type="password" name="password" placeholder="Пароль"
                                       value={password} onChange={(e) => {
                                    this.onChange('password', e.target.value)
                                }}/>
                                <span className="focus-input100">
                                    <Icon type="lock"/>
                                </span>
                            </div>

                            <div className="container-login100-form-btn">
                                <button className="login100-form-btn">
                                    Войти
                                </button>
                            </div>

                            <div className="text-center p-t-90">
                                <a className="txt1" href="#">
                                    Забыли пароль?
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
