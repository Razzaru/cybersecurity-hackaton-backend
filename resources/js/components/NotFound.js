import React, { Component } from 'react';

export default class NotFound extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Example Component</div>

                            <div className="card-body">
                                404
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
