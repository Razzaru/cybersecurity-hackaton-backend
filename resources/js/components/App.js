import React, {Component} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import _ from 'lodash';
import {createBrowserHistory} from 'history'

import PrivateLayout from '../layouts/PrivateLayout';
import PublicLayout from '../layouts/PublicLayout';
import SessionLayout from "../layouts/SessionLayout";

import privateRoutes from '../routes/privateRoutes';
import publicRoutes from '../routes/publicRoutes';
import sessionRoutes from '../routes/sessionRoutes';

import {Router} from "react-router";

import NotFound from './NotFound';
import Forbidden from './Forbidden';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
        };
    }

    render() {
        const user = window.user || this.state.user || null;

        return (
            <Switch>
                {_.map(publicRoutes, (route, key) => {
                    const {component, path} = route;
                    return (
                        <Route
                            exact
                            path={path}
                            key={key}
                            render={(route) => <PublicLayout component={component} route={route} user={user}/>}
                        />
                    )
                })}

                {_.map(privateRoutes, (route, key) => {
                    const {component, path} = route;

                    return (
                        <Route
                            exact
                            path={path}
                            key={key}
                            render={(route) =>
                                user ? (
                                    <PrivateLayout component={component}
                                                   routeKey={key}
                                                   route={route}
                                                   user={user}
                                    />
                                ) : (
                                    <Redirect to={sessionRoutes.Login.path}/>
                                )
                            }
                        />
                    )
                })}

                {_.map(sessionRoutes, (route, key) => {
                    const {component, path} = route;
                    return (
                        <Route
                            exact
                            path={path}
                            key={key}
                            render={(route) =>
                                user ? (
                                    <Redirect to={privateRoutes.Dashboard.path}/>
                                ) : (
                                    <SessionLayout component={component} route={route} setUser={(user) => {
                                        this.setState(() => ({
                                            user,
                                        }))
                                    }}/>
                                )
                            }
                        />
                    )
                })}

                <Route component={NotFound}/>
            </Switch>
        );
    }
}
