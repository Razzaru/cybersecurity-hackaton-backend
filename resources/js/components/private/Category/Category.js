import React from 'react';
import axios from "axios";
import {Form, Button, Input, Popconfirm, Table} from "antd";
import {Link} from "react-router-dom";

export default class Category extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
        };
    }

    componentWillMount() {
        this.fetchCategory();
    }

    fetchCategory() {
        console.log(this.props);
        axios.get(`/api/categories/`)
            .then(response => {
                this.setState(() => ({
                    categories: response.data.categories,
                }))
            })
            .catch(() => {

            });
    }

    render() {
        return (
            <Table
                dataSource={this.getDataSource()}
                columns={columns}
            />
        )
    }
}
