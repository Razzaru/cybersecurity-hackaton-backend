import React from 'react';
import axios from "axios";
import {Form, Button, Input, Popconfirm, Table} from "antd";
import {Link} from "react-router-dom";

const columns = [
    {
        title: 'id',
        dataIndex: 'id',
        render: (text, record) => <Link to={`/admin/category/${record.id}`}>{record.id}</Link>
    },
    {
        title: 'Название',
        dataIndex: 'name',
        width: '30%',
        editable: true,
    },
    {
        title: '',
        dataIndex: 'operation',
        render: (text, record) =>
            <Popconfirm title="Вы уверены, что хотите удалить?" onConfirm={() => this.handleDelete(record.key)}>
                <a href="javascript:;">Удалить</a>
            </Popconfirm>
    },
];

export default class CategoryView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
        };
    }

    componentWillMount() {
        this.fetchCategories();
    }

    fetchCategories() {
        axios.get(`/api/categories`)
            .then(response => {
                this.setState(() => ({
                    categories: response.data.categories,
                }))
            })
            .catch(() => {

            });
    }

    getDataSource() {
        const { categories } = this.state;

        return categories.map(category => ({
            key: category.id,
            id: category.id,
            name: category.name,
        }));
    }

    render() {
        return (
            <Table
                dataSource={this.getDataSource()}
                columns={columns}
            />
        )
    }
}
