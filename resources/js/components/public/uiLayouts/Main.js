import React from 'react';

const Layout = props => (
    <div className="landing-layout">
        {props.children}
    </div>
);

export default Layout;