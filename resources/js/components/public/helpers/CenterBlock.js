import React from 'react';

const CenterBlock = props => (
    <div className="center-block" style={props.style}>
        {props.children}
    </div>
);

export default CenterBlock;