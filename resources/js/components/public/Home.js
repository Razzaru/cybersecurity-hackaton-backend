import React, {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faShieldAlt} from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom'

import Layout from "./uiLayouts/Main";
import CenterBlock from "./helpers/CenterBlock";

const Home = () => {
    const [isToggle, toggle] = useState(false);

    return (
        <Layout>
            <CenterBlock style={{width: '40%'}}>
                <img style={{position: 'absolute', left: '-200px', width: '200px', height: '200px'}}
                     src="/vectors/main-logo.svg"/>
                <h1 className="landing-header">CYBERIAN PROJECT</h1>
                <div className="landing-description-block">
                    <ul className="landing-list landing-text">
                        <li>Мы - команда, которая научит беспокоиться о данных.</li>
                        <li>Мы взломаем вас, а затем научим, как этого избежать.</li>
                        <li>Мы - Cyberian Project.</li>
                    </ul>
                    <h3 className="landing-second-header">Проверьте данные на прочность!</h3>
                    <p className="landing-text landing-text-small">
                        Закажите частную или корпоративную проверку осведомленности людей о кибербезопасности, а так же
                        скачайте наше бесплатное приложение с уроками, тестами и играми по различным темам, касающимся
                        защиты данных.
                    </p>
                    <span className="landing-link landing-toggle"
                          onClick={() => toggle(!isToggle)}
                    >Как это работает?
                    <p
                        className="landing-toggle-text"
                        style={{visibility: isToggle ? 'visible' : 'hidden'}}>
                        Наши специалисты всеми возможными способами пытаются взломать и похитить защищенные данные
                        клиента, в следствие выявляются слабые стороны защиты. После этого мы рассказываем вам об этих
                        проблемах и предлагаем эффективные пути решения. <br/>Все просто!
                    </p>
                </span>
                </div>
                <div className="landing-button-block">
                    <div className="landing-button-block-media">
                        <img style={{width: '159px', height: '58px', cursor: 'pointer'}}
                             src="/vectors/download-on-the-app-store-apple.svg"/>
                        <img style={{width: '159px', height: '58px', cursor: 'pointer'}}
                             src="/vectors/google-play-badge.svg"/>
                        <Link to="/price-list">
                            <div className="landing-button-block-media-link"><FontAwesomeIcon
                                className="landing-button-block-media-link-icon" style={{fontSize: '24px'}}
                                icon={faShieldAlt}/><span>Наши услуги</span></div>
                        </Link>
                    </div>
                </div>
            </CenterBlock>
        </Layout>
    );
};

export default Home;