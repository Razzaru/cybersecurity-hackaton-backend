import React from 'react';
import Layout from './uiLayouts/Main';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheck} from '@fortawesome/free-solid-svg-icons'

const PriceList = () => {
    return (
        <Layout>
            <div style={{margin: '0 auto', display: 'table', padding: '30px', color: '#d1d1d1'}}>
                <div style={{width: '550px', padding: '0 20px', height: '650px', float: 'left', borderRight: '1px solid #d1d1d1'}}>
                    <h1 style={{marginBottom: '60px', textAlign: 'center'}}>Частным клиентам</h1>
                    <div style={{margin: '10px 0', borderRadius: '5px'}}>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка защищенности персональных данных</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка на защищенность от фишинга</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка на выманивание персональных данных</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка возможности похищения денег с банковской карты</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Выявление слабых мест кибер-защиты</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Предоставление мобильного приложения для обучения кибербезопасности</p>
                        <div className="price-list-button">Отправить запрос</div>
                    </div>
                </div>
                <div style={{width: '550px', padding: '0 20px', height: '650px', float: 'left'}}>
                    <h1 style={{marginBottom: '60px', textAlign: 'center'}}>Корпоративным клиентам</h1>
                    <div style={{margin: '10px 0', borderRadius: '5px'}}>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка защищенности персональных и рабочих данных сотрудников</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка на защищенность от фишинга</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка защищенности почтовой сети компании</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Выявление слабых мест кибер-защиты</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Предоставление мобильного приложения для обучения кибербезопасности</p>
                        <p style={{display: 'flex', alignItems: 'center', fontSize: '22px', height: '50px'}}><FontAwesomeIcon style={{color: '#27AE60',display: 'block', height: '100%', float: 'left', marginRight: '10px'}} icon={faCheck}/> Проверка возможности проникновения в сеть компании через ПК сотрудников</p>
                        <div className="price-list-button">Отправить запрос</div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default PriceList