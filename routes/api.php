<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::get('login/vk', [\App\Http\Controllers\Api\AuthController::class, 'redirectToProvider']);
    Route::get('login/vk/callback', [\App\Http\Controllers\Api\AuthController::class, 'handleProviderCallback']);
    Route::post('signup', [\App\Http\Controllers\Api\AuthController::class, 'signup']);


    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
        Route::get('user', [\App\Http\Controllers\Api\AuthController::class, 'user']);
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('lessons')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\LessonController::class, 'get']);
        Route::get('/{id}', [\App\Http\Controllers\Api\LessonController::class, 'getOne']);
    });

    Route::prefix('tests')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\TestController::class, 'get']);
        Route::get('{id}', [\App\Http\Controllers\Api\TestController::class, 'getOne']);
    });

    Route::prefix('categories')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\TestController::class, 'get']);
        Route::get('{id}', [\App\Http\Controllers\Api\TestController::class, 'getOne']);
        Route::post('/create', [\App\Http\Controllers\Api\TestController::class, 'create']);
        Route::post('update/{id}', [\App\Http\Controllers\Api\TestController::class, 'update']);
        Route::post('delete/{id}', [\App\Http\Controllers\Api\TestController::class, 'delete']);
    });
});
